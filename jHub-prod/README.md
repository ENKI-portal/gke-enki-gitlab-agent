# JupyterHub installation for production
JupyterHub is the server infrastructure that supports the ENKI portal. Before upgrading the Hub or testing new features, it is a good idea to instantiate a testing server for evaluation, fully configured for public access. Once the testing server is instantiated and verified, upgrade the production server. Use the procedures below and the *jhub-config.yaml* file (located in this subdirectory) to accomplish these tasks.

If you are upgrading (not installing) the server, see [To configure or upgrade the JupyterHub for production](#to-configure-or-upgrade-the-jupyterhub-for-production).

##### To perform a fresh installation of the server:

1. In Google Cloud Shell, execute the command:
    ```
    helm upgrade --cleanup-on-fail --install jhub jupyterhub/jupyterhub \
    --namespace jhub --create-namespace --version=1.1.3
    ```
    The argument to `--version` should be whichever version of the Helm chart you wish to use. See https://jupyterhub.github.io/helm-chart/ for a list of available chart versions and the JupyterHub app version supported by each Helm chart.
    
    The output of this command looks like:
    ```
    Release "jhub" does not exist. Installing it now.
    NAME: jhub
    LAST DEPLOYED: Thu Oct  7 17:04:28 2021
    NAMESPACE: jhub
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Thank you for installing JupyterHub!

    Your release is named "testhub" and installed into the namespace "jhub".

    You can check whether the hub and proxy are ready by running:

     kubectl --namespace=jhub get pod

    and watching for both those pods to be in status 'Running'.

    You can find the public (load-balancer) IP of JupyterHub by running:

      kubectl -n jhub get svc proxy-public -o jsonpath='{.status.loadBalancer.ingress[].ip}'

    It might take a few minutes for it to appear!

    To get full information about the JupyterHub proxy service run:

      kubectl --namespace=jhub get svc proxy-public

    If you have questions, please:

      1. Read the guide at https://z2jh.jupyter.org
      2. Ask for help or chat to us on https://discourse.jupyter.org/
      3. If you find a bug please report it at https://github.com/jupyterhub/zero-to-jupyterhub-k8s/issues
    ```
1. Test to make sure that the pods have been generated  and are running:
    ```
    kubectl get pod --namespace jhub
    ```
    Typical output:
    ```
    NAME                              READY   STATUS    RESTARTS   AGE
    continuous-image-puller-7c6lr     1/1     Running   0          94s
    continuous-image-puller-858pc     1/1     Running   0          94s
    continuous-image-puller-j5d7w     1/1     Running   0          93s
    continuous-image-puller-nkrwr     1/1     Running   0          93s
    continuous-image-puller-x7wvq     1/1     Running   0          94s
    hub-5c755fdb8-8qrkv               1/1     Running   0          93s
    proxy-f694dc78b-cq8tb             1/1     Running   0          93s
    user-scheduler-8484b779cb-dthcj   1/1     Running   0          93s
    user-scheduler-8484b779cb-h62bl   1/1     Running   0          93s
    ```
1. Test for service generation, and get the IP of the generated load balancer:
    ```
    kubectl get service --namespace jhub
    ```
    Typical output:
    ```
    NAME           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
    hub            ClusterIP      10.35.246.151   <none>          8081/TCP       3m28s
    proxy-api      ClusterIP      10.35.243.139   <none>          8001/TCP       3m28s
    proxy-public   LoadBalancer   10.35.245.226   35.247.85.188   80:32544/TCP   3m28s
    ```
1. Map this IP address to a domain name (at GoDaddy or equivalent) using an *a-record*.

1. Define or update the following CI environment variables (Settings > CI/CD > Variables):
    - JUPYTERHUB_ADMIN_USER  
      > GitLab username designated as the Hub administrator
    - JUPYTERHUB_AUTH_GITLAB_CLIENT_ID  
      > The GitLab client ID authorization token generated from the User Settings menu (not the project menu) at User Settings > Applications. Generate a new application access token with *read_user*, *read_api*, *openid*, *profile*, and *email* permissions. 
    - JUPYTERHUB_AUTH_GITLAB_CLIENT_SECRET  
      > The client authorization secret generated along with the client ID, as above
    - JUPYTERHUB_HOST  
      > The DNS name of the production server that is mapped to the load balancer IP at the domain provider (i.e., GoDaddy.com)
    - JUPYTERHUB_LETSENCRYPT_EMAIL  
      > The valid email address to be registered with the letsencrypt certificate provider to be issued to the load balancer/domain address for https connections
    - JUPYTERHUB_LOAD_BALANCER_IP  
      > The load balancer External IP generated for the proxy-public pod when the Hub is installed (see above)
 
1. Configure the JupyterHub for production. See [To configure or upgrade the JupyterHub for production](#to-configure-or-upgrade-the-jupyterhub-for-production).


##### To configure or upgrade the JupyterHub for production:

- Run the command below, which provides chart configuration values in the YAML file *jhub-config.yaml*.   
That file relies on substitution of values of the environment variables specified above prior to application. You can perform this substitution either manually or in GitLab CI by filtering the file first through the Linux command `envsubst`.  
    - If using manual substitution, use the `helm upgrade` command:
      ```
      helm upgrade --cleanup-on-fail jhub jupyterhub/jupyterhub --namespace jhub \
      --version=1.1.3 --timeout 10m0s --values jhub-config.yaml
      ```
    - If performing this upgrade in GitLab CI, use the command:
      ```
      envsubst < jhub-config.yaml | helm upgrade --cleanup-on-fail jhub jupyterhub/jupyterhub \
      --namespace jhub --version=1.1.3 --timeout 10m0s --values -
      ```
The production server is now ready for testing and evaluation.

## Tearing down the production server
Tearing down the production server is an easy process, but be aware that it removes all user permanent storage. Make sure to back up user data first so that you can reapply it when another server is activated. 

##### To tear down the production server:

1. Back up user data so that you can reapply it when another server is activated.

1. In Google Cloud Shell, execute the `helm` command:
   ```
   helm delete jhub --namespace jhub
   ```
2. Delete the namespace to remove unwanted configuration secrets:
   ```
   kubectl delete namespace jhub
   ```
And that's it!

