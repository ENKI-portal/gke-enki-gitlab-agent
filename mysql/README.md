# MySQL installation 

Follow the procedure below to install MySQL and to expose the service using a load balancer mapped to mysql.enki-portal.org:3306. The procedure utilizes YAML files in this folder. DNS forwarding is configured at GoDaddy.com.


##### To install MySQL and expose the service using a load balancer mapped to mysql.enki-portal.org:3306:

1. In Google Cloud Shell, create a namespace for the installation:
    ```
    kubectl create namespace mysql
    ```
1. Create a secret that contains and masks the root password for the database server:
    ```
    kubectl create secret generic mysql-creds --from-literal=ROOT_PASSWORD=changeme -n mysql
    ```
1. Apply the file *1-mysql-config.yaml* to create a *config-map* specifying version and configuration defaults:  
    ```
    kubectl -n mysql apply -f 1-mysql-config.yaml
    ```
1. Create a persistent storage claim of 2GB for permanent data storage by applying the file *2-mysql-pv.yaml*:   
    ```
    kubectl -n mysql apply -f 2-mysql-pv.yaml
    ```
1. Deploy the service by applying the file *3-mysql-deployment.yaml*: 
    ```
    kubectl -n mysql apply -f 3-mysql-deployment.yaml
    ```
    Note that this deployment does not allocate duplicate mirrored replica storage sets. This installation is meant for low volume usage. It can be scaled, but using a preconfigured database access point on Google Cloud that is independent of the Kubernetes installation is probably better.
1. Create a MySQL service, and expose the service using a load balancer by applying the file *4-mysql-svc.yaml*:
    ```
    kubectl -n mysql apply -f 4-mysql-svc.yaml
    ```
    Note the external IP generated (examine the mysql service), which opens port 3306 and routes traffic to the MySQL pod. 
1. Map the generated IP to mysql.enki-portal.org at GoDaddy.com.

You can access this MySQL installation from *MySQL Workbench* or similar running from a Desktop system.

