
# Prometheus installation

Prometheus is a monitoring service for Kubernetes clusters and their application pods. You install Prometheus using Google Cloud Shell with a Helm chart (kube-prometheus-stack). 

Currently (Oct 2021), installation by generating a YAML manifest and deploying with GitLab Kubernetes Agent fails because the agent cannot properly configure the required webhook pods.  

**To install Prometheus:**

1. Create a namespace named *monitoring*:
   ```
   kubectl create namespace monitoring
   ```
1. Install Helm into the *monitoring* namespace by executing the `helm` commands in Google Cloud Shell:
   ```
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   helm repo update
   helm install -n monitoring helm-stack prometheus-community/kube-prometheus-stack
   ```
    These commands utilize default values for the stack, which installed **alertmanager**, **prometheus** and **grafana**.  After installation, port forwarding works for access to Grafana; choose a Prometheus source according to the internal cluster IP and Port 9090. 

**To uninstall Prometheus:**  
- Run the command:  
    ```
    helm uninstall prometheus-stack -n monitoring
    ```  

**To delete the namespace:**  
- Run the command:
    ```
    kubectl delete ns monitoring
    ```


