# JupyterHub installation for testing
JupyterHub is the server infrastructure that supports the ENKI portal. Before upgrading the Hub or testing new features, it is a good idea to instantiate a testing server for evaluation, fully configured for public access. Use the procedure below and the associated *test-jhub-config.yaml* file to accomplish this task.

##### To instantiate a testing server:


1. In Google Cloud Shell, execute the command:
    ```
    helm upgrade --cleanup-on-fail --install testhub jupyterhub/jupyterhub \
    --namespace testhub --create-namespace --version=1.1.3
    ```
    The argument to `--version` should be whichever version of the Helm chart you wish to test. See https://jupyterhub.github.io/helm-chart/ for a list of available chart versions and the JupyterHub app version supported by each Helm chart.
    
    The output of this command looks like:
    ```
    Release "testhub" does not exist. Installing it now.
    NAME: testhub
    LAST DEPLOYED: Thu Oct  7 17:04:28 2021
    NAMESPACE: testhub
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    Thank you for installing JupyterHub!

    Your release is named "testhub" and installed into the namespace "testhub".

    You can check whether the hub and proxy are ready by running:

     kubectl --namespace=testhub get pod

    and watching for both those pods to be in status 'Running'.

    You can find the public (load-balancer) IP of JupyterHub by running:

      kubectl -n testhub get svc proxy-public -o jsonpath='{.status.loadBalancer.ingress[].ip}'

    It might take a few minutes for it to appear!

    To get full information about the JupyterHub proxy service run:

      kubectl --namespace=testhub get svc proxy-public

    If you have questions, please:

      1. Read the guide at https://z2jh.jupyter.org
      2. Ask for help or chat to us on https://discourse.jupyter.org/
      3. If you find a bug please report it at https://github.com/jupyterhub/zero-to-jupyterhub-k8s/issues
    ```
1. Test to make sure that the pods have been generated  and are running:
    ```
    kubectl get pod --namespace testhub
    ```
    Typical output:
    ```
    NAME                              READY   STATUS    RESTARTS   AGE
    continuous-image-puller-7c6lr     1/1     Running   0          94s
    continuous-image-puller-858pc     1/1     Running   0          94s
    continuous-image-puller-j5d7w     1/1     Running   0          93s
    continuous-image-puller-nkrwr     1/1     Running   0          93s
    continuous-image-puller-x7wvq     1/1     Running   0          94s
    hub-5c755fdb8-8qrkv               1/1     Running   0          93s
    proxy-f694dc78b-cq8tb             1/1     Running   0          93s
    user-scheduler-8484b779cb-dthcj   1/1     Running   0          93s
    user-scheduler-8484b779cb-h62bl   1/1     Running   0          93s
    ```
1. Test for service generation, and get the IP of the generated load balancer:
    ```
    kubectl get service --namespace testhub
    ```
    Typical output:
    ```
    NAME           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
    hub            ClusterIP      10.35.246.151   <none>          8081/TCP       3m28s
    proxy-api      ClusterIP      10.35.243.139   <none>          8001/TCP       3m28s
    proxy-public   LoadBalancer   10.35.245.226   35.247.85.188   80:32544/TCP   3m28s
    ```
1. Map this IP address to a domain name (at GoDaddy or equivalent) using an *a-record*.

1. Define or update the following CI environment variables (Settings > CI/CD > Variables):
    - TESTHUB_ADMIN_USER  
      > GitLab username designated as the Hub administrator
    - TESTHUB_AUTH_GITLAB_CLIENT_ID  
      > The GitLab client ID authorization token generated from the User Settings menu (not the project menu) at User Settings > Applications. Generate a new application access token with *read_user*, *read_api*, *openid*, *profile*, and *email* permissions. 
    - TESTHUB_AUTH_GITLAB_CLIENT_SECRET  
      > The client authorization secret generated along with the client ID, as above
    - TESTHUB_HOST  
      > The DNS name of the testing server that is mapped to the load balancer IP at the domain provider (i.e., GoDaddy.com)
    - TESTHUB_LETSENCRYPT_EMAIL  
      > The valid email address to be registered with the letsencrypt certificate provider to be issued to the load balancer/domain address for https connections
    - TESTHUB_LOAD_BALANCER_IP  
      > The load balancer External IP generated for the proxy-public pod when the Hub is installed (see above)
 
1. Configure/Upgrade JupyterHub for production by using the command below, which provides chart configuration values in the YAML file *test-jhub-config.yaml*.  
That file relies on substitution of values of the environment variables specified above prior to application. You can perform this substitution either manually or in GitLab CI by filtering the file first through the Linux command `envsubst`.  
    - If using manual substitution, use the `helm upgrade` command:
      ```
      helm upgrade --cleanup-on-fail testhub jupyterhub/jupyterhub --namespace testhub \
      --version=1.1.3 --timeout 10m0s --values test-jhub-config.yaml
      ```
    - If performing this upgrade in GitLab CI, use the command:
      ```
      envsubst < test-jhub-config.yaml | helm upgrade --cleanup-on-fail testhub jupyterhub/jupyterhub \
      --namespace testhub --version=1.1.3 --timeout 10m0s --values -
      ```
The testing Hub is now ready for testing and evaluation.

## Tearing down the testing server
Tearing down the testing server is an easy process that you should perform as soon as the upgrade is verified and the production server is mirrored to the testing server.

##### To tear down the testing server:
1. In Google Cloud Shell, execute the `helm` command:
   ```
   helm delete testhub --namespace testhub
   ```
1. Delete the namespace to remove unwanted configuration secrets:
   ```
   kubectl delete namespace testhub
   ```
And that's it!

