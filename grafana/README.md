# Grafana installation
Grafana is the graphical user interface to the Prometheus Kubernetes cluster monitoring package. It installs during installation of the kube-prometheus stack (See README in the *kube-prometheus* subfolder).  

The Grafana service is exposed using the Ingress controller and is secured using cert-manager, which generates a certificate using letsencrypt.

##### To set up Grafana:  
1. In Google Cloud Shell, expose the Grafana service by applying the YAML file *monitoring-ingress.yaml*:
    ```
    kubectl --namespace monitoring apply -f monitoring-ingress.yaml
    ```
1. Access the service at https://cluster.enki-portal.org/.  
    The administration username and password are provided in this repository's CI environment variables (Settings > CI/CD > Variables) GRAFANA_ADMIN_USER and GRAFANA_ADMIN_PASSWORD.