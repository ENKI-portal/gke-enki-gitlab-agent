# Gitlab Cluster Integration Notes

## 10/12/21

### Create a jupyterhub installation for testing

- create test-jhub-config.yaml
```
# This file can update the JupyterHub Helm chart's default configuration values.
#
# For reference see the configuration reference and default values, but make
# sure to refer to the Helm chart version of interest to you!
#
# Introduction to YAML:     https://www.youtube.com/watch?v=cdLNKUoMc6c
# Chart config reference:   https://zero-to-jupyterhub.readthedocs.io/en/stable/resources/reference.html
# Chart default values:     https://github.com/jupyterhub/zero-to-jupyterhub-k8s/blob/HEAD/jupyterhub/values.yaml
# Available chart versions: https://jupyterhub.github.io/helm-chart/
#
```
- apply the yaml file with helm in the Google Cloud Shell
```
helm upgrade --cleanup-on-fail --install testhub jupyterhub/jupyterhub --namespace testhub --create-namespace --version=1.1.3 --values test-jhub-config.yaml
```
output:
```
Release "testhub" does not exist. Installing it now.
NAME: testhub
LAST DEPLOYED: Thu Oct  7 17:04:28 2021
NAMESPACE: testhub
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Thank you for installing JupyterHub!

Your release is named "testhub" and installed into the namespace "testhub".

You can check whether the hub and proxy are ready by running:

 kubectl --namespace=testhub get pod

and watching for both those pods to be in status 'Running'.

You can find the public (load-balancer) IP of JupyterHub by running:

  kubectl -n testhub get svc proxy-public -o jsonpath='{.status.loadBalancer.ingress[].ip}'

It might take a few minutes for it to appear!

To get full information about the JupyterHub proxy service run:

  kubectl --namespace=testhub get svc proxy-public

If you have questions, please:

  1. Read the guide at https://z2jh.jupyter.org
  2. Ask for help or chat to us on https://discourse.jupyter.org/
  3. If you find a bug please report it at https://github.com/jupyterhub/zero-to-jupyterhub-k8s/issues
```
- test for pod generation
```
kubectl get pod --namespace testhub
```
output:
```
NAME                              READY   STATUS    RESTARTS   AGE
continuous-image-puller-7c6lr     1/1     Running   0          94s
continuous-image-puller-858pc     1/1     Running   0          94s
continuous-image-puller-j5d7w     1/1     Running   0          93s
continuous-image-puller-nkrwr     1/1     Running   0          93s
continuous-image-puller-x7wvq     1/1     Running   0          94s
hub-5c755fdb8-8qrkv               1/1     Running   0          93s
proxy-f694dc78b-cq8tb             1/1     Running   0          93s
user-scheduler-8484b779cb-dthcj   1/1     Running   0          93s
user-scheduler-8484b779cb-h62bl   1/1     Running   0          93s
```
- test for service generation (and get IP of load balancer to expose on GoDaddy):
```
kubectl get service --namespace testhub
```
output:
```
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
hub            ClusterIP      10.35.246.151   <none>          8081/TCP       3m28s
proxy-api      ClusterIP      10.35.243.139   <none>          8001/TCP       3m28s
proxy-public   LoadBalancer   10.35.245.226   35.247.85.188   80:32544/TCP   3m28s
```
- the jupyterhub server may be upgraded using the command:
```
helm upgrade --cleanup-on-fail testhub jupyterhub/jupyterhub --namespace testhub --version=1.1.3 --timeout 10m0s --values test-jhub-config.yaml
```
An updated test-jhub-config.yaml requires credentials from Gitlab to authorize logins, wjhich are generated on the Gitlab user interface at "Gitlab Application authorization."  These were created:
```
ENKI Test Server Authorization
https://testserver.enki-portal.org/hub/oauth_callback
```
- revised test-jhub-config.yaml
```
# This file can update the JupyterHub Helm chart's default configuration values.
#
# For reference see the configuration reference and default values, but make
# sure to refer to the Helm chart version of interest to you!
#
# Introduction to YAML:     https://www.youtube.com/watch?v=cdLNKUoMc6c
# Chart config reference:   https://zero-to-jupyterhub.readthedocs.io/en/stable/resources/reference.html
# Chart default values:     https://github.com/jupyterhub/zero-to-jupyterhub-k8s/blob/HEAD/jupyterhub/values.yaml
# Available chart versions: https://jupyterhub.github.io/helm-chart/
#
singleuser:
  defaultUrl: "/lab"
  extraEnv:
    JUPYTERHUB_SINGLEUSER_APP: "jupyter_server.serverapp.ServerApp"
  lifecycleHooks:
    postStart:
      exec:
        command:
          - "sh"
          - "-c"
          - >
            gitpuller https://gitlab.com/enki-portal/ThermoEngine master ThermoEngine;
            gitpuller https://gitlab.com/ENKI-portal/enki_workshops.git master ENKI_Workshops
  memory:
    limit: 2G
    guarantee: 1G
  cpu:
    limit: 1
    guarantee: .2
  extraFiles:
    # jupyter_notebook_config reference: https://jupyter-notebook.readthedocs.io/en/stable/config.html
    jupyter_notebook_config.json:
      mountPath: /etc/jupyter/jupyter_notebook_config.json
      # data is a YAML structure here but will be rendered to JSON file as our
      # file extension is ".json".
      data:
        MappingKernelManager:
          # cull_idle_timeout: timeout (in seconds) after which an idle kernel is
          # considered ready to be culled
          cull_idle_timeout: 1200 # default: 0
          # cull_interval: the interval (in seconds) on which to check for idle
          # kernels exceeding the cull timeout value
          cull_interval: 120 # default: 300
          # cull_connected: whether to consider culling kernels which have one
          # or more connections
          cull_connected: true # default: false
          # cull_busy: whether to consider culling kernels which are currently
          # busy running some code
          cull_busy: false # default: false
  image:
    name: registry.gitlab.com/enki-portal/thermoengine
    tag: master
    pullPolicy: Always

scheduling:
  userScheduler:
    enabled: true
  podPriority:
    enabled: true
  userPlaceholder:
    enabled: true
    replicas: 3
  userPods:
    nodeAffinity:
      matchNodePurpose: require

cull:
  enabled: true
  timeout: 3600
  every: 300

proxy:
  https:
    enabled: true
    hosts:
      - testserver.enki-portal.org
    letsencrypt:
      contactEmail: ghiorso@ofm-research.org
  service:
    loadBalancerIP: 35.247.85.188

hub:
  config:
    Authenticator:
      admin_users: ['ghiorso']
    GitLabOAuthenticator:
      client_id: "9c8ab540cabcd1779827149a5287ade46dafafab0497d2b21279c2be5dc1ada4"
      client_secret: "dc18bfa2cd829b2a27ac123140f9f3d3f6023152760bea96b27ecee5b429c5b3"
      oauth_callback_url: “https://testserver.enki-portal.org/hub/oauth_callback”
    JupyterHub:
      admin_access: true
      authenticator_class: gitlab
  extraConfig:
    10-second-config: |
      from oauthenticator.gitlab import GitLabOAuthenticator
      c.JupyterHub.authenticator_class = GitLabOAuthenticator
      c.GitLabOAuthenticator.scope = ['read_user']
      c.GitLabOAuthenticator.oauth_callback_url = 'https://testserver.enki-portal.org/hub/oauth_callback'
      c.GitLabOAuthenticator.client_id = '9c8ab540cabcd1779827149a5287ade46dafafab0497d2b21279c2be5dc1ada4'
      c.GitLabOAuthenticator.client_secret = 'dc18bfa2cd829b2a27ac123140f9f3d3f6023152760bea96b27ecee5b429c5b3'
    20-templates: |
      c.JupyterHub.template_paths = ['/etc/custom/jupyterhub_custom/templates']
  extraEnv:
    GITLAB_URL: https://gitlab.com
  initContainers:
    - name: git-clone-templates
      image: alpine/git
      args:
        - clone
        - --single-branch
        - --branch=master
        - --depth=1
        - --
        - https://gitlab.com/ENKI-portal/jupyterhub_custom.git
        - /etc/custom/jupyterhub_custom
      securityContext:
        runAsUser: 0
      volumeMounts:
        - name: custom-templates
          mountPath: /etc/custom/jupyterhub_custom
  extraVolumes:
    - name: custom-templates
      emptyDir: {}
  extraVolumeMounts:
    - name: custom-templates
      mountPath: /etc/custom/jupyterhub_custom
```
- to tear down the testing server:
```
helm delete testhub --namespace testhub
kubectl delete namespace testhub
```
- testing reveals that the upgrade yaml for the production server should be:
```
# This file can update the JupyterHub Helm chart's default configuration values.
#
# For reference see the configuration reference and default values, but make
# sure to refer to the Helm chart version of interest to you!
#
# Introduction to YAML:     https://www.youtube.com/watch?v=cdLNKUoMc6c
# Chart config reference:   https://zero-to-jupyterhub.readthedocs.io/en/stable/resources/reference.html
# Chart default values:     https://github.com/jupyterhub/zero-to-jupyterhub-k8s/blob/HEAD/jupyterhub/values.yaml
# Available chart versions: https://jupyterhub.github.io/helm-chart/
#
singleuser:
  defaultUrl: "/lab"
  extraEnv:
    JUPYTERHUB_SINGLEUSER_APP: "jupyter_server.serverapp.ServerApp"
  lifecycleHooks:
    postStart:
      exec:
        command:
          - "sh"
          - "-c"
          - >
            gitpuller https://gitlab.com/enki-portal/ThermoEngine master ThermoEngine;
            gitpuller https://gitlab.com/ENKI-portal/enki_workshops.git master ENKI_Workshops
  memory:
    limit: 2G
    guarantee: 1G
  cpu:
    limit: 1
    guarantee: .2
  extraFiles:
    # jupyter_notebook_config reference: https://jupyter-notebook.readthedocs.io/en/stable/config.html
    jupyter_notebook_config.json:
      mountPath: /etc/jupyter/jupyter_notebook_config.json
      # data is a YAML structure here but will be rendered to JSON file as our
      # file extension is ".json".
      data:
        MappingKernelManager:
          # cull_idle_timeout: timeout (in seconds) after which an idle kernel is
          # considered ready to be culled
          cull_idle_timeout: 1200 # default: 0
          # cull_interval: the interval (in seconds) on which to check for idle
          # kernels exceeding the cull timeout value
          cull_interval: 120 # default: 300
          # cull_connected: whether to consider culling kernels which have one
          # or more connections
          cull_connected: true # default: false
          # cull_busy: whether to consider culling kernels which are currently
          # busy running some code
          cull_busy: false # default: false
  image:
    name: registry.gitlab.com/enki-portal/thermoengine
    tag: master
    pullPolicy: Always

scheduling:
  userScheduler:
    enabled: true
  podPriority:
    enabled: true
  userPlaceholder:
    enabled: true
    replicas: 3
  userPods:
    nodeAffinity:
      matchNodePurpose: require

cull:
  enabled: true
  timeout: 3600
  every: 300

proxy:
  https:
    enabled: true
    hosts:
      - server.enki-portal.org
    letsencrypt:
      contactEmail: ghiorso@ofm-research.org
  service:
    loadBalancerIP: 35.227.191.142

hub:
  config:
    Authenticator:
      admin_users: ['ghiorso']
    GitLabOAuthenticator:
      client_id: "5d56beb18d6d6c85b6fe1bb7b2023a15f72e77f1fedd0de625c03c5f59401e7d"
      client_secret: "4fa498951acb705bbc4d8f7c4b695e88f1ffa662084b85c75fe7778e24098a2b"
      oauth_callback_url: “https://server.enki-portal.org/hub/oauth_callback”
    JupyterHub:
      admin_access: true
      authenticator_class: gitlab
  extraConfig:
    10-second-config: |
      from oauthenticator.gitlab import GitLabOAuthenticator
      c.JupyterHub.authenticator_class = GitLabOAuthenticator
      c.GitLabOAuthenticator.scope = ['read_user']
      c.GitLabOAuthenticator.oauth_callback_url = 'https://server.enki-portal.org/hub/oauth_callback'
      c.GitLabOAuthenticator.client_id = '5d56beb18d6d6c85b6fe1bb7b2023a15f72e77f1fedd0de625c03c5f59401e7d'
      c.GitLabOAuthenticator.client_secret = '4fa498951acb705bbc4d8f7c4b695e88f1ffa662084b85c75fe7778e24098a2b'
    20-templates: |
      c.JupyterHub.template_paths = ['/etc/custom/jupyterhub_custom/templates']
  extraEnv:
    GITLAB_URL: https://gitlab.com
  initContainers:
    - name: git-clone-templates
      image: alpine/git
      args:
        - clone
        - --single-branch
        - --branch=master
        - --depth=1
        - --
        - https://gitlab.com/ENKI-portal/jupyterhub_custom.git
        - /etc/custom/jupyterhub_custom
      securityContext:
        runAsUser: 0
      volumeMounts:
        - name: custom-templates
          mountPath: /etc/custom/jupyterhub_custom
  extraVolumes:
    - name: custom-templates
      emptyDir: {}
  extraVolumeMounts:
    - name: custom-templates
      mountPath: /etc/custom/jupyterhub_custom
```
which is applied as:
```
helm upgrade --cleanup-on-fail jhub jupyterhub/jupyterhub  --namespace jhub --version=1.1.3 --timeout 10m0s --values jhub-config.yaml
```
output:
```
Release "jhub" has been upgraded. Happy Helming!
NAME: jhub
LAST DEPLOYED: Sat Oct  9 22:54:11 2021
NAMESPACE: jhub
STATUS: deployed
REVISION: 15
TEST SUITE: None
NOTES:
Thank you for installing JupyterHub!

Your release is named "jhub" and installed into the namespace "jhub".

You can check whether the hub and proxy are ready by running:

 kubectl --namespace=jhub get pod

and watching for both those pods to be in status 'Running'.

You can find the public (load-balancer) IP of JupyterHub by running:

  kubectl -n jhub get svc proxy-public -o jsonpath='{.status.loadBalancer.ingress[].ip}'

It might take a few minutes for it to appear!

To get full information about the JupyterHub proxy service run:

  kubectl --namespace=jhub get svc proxy-public

If you have questions, please:

  1. Read the guide at https://z2jh.jupyter.org
  2. Ask for help or chat to us on https://discourse.jupyter.org/
  3. If you find a bug please report it at https://github.com/jupyterhub/zero-to-jupyterhub-k8s/issues

```
Note: Had to go in and delete the managed pod for autohttps workload pod (which manages the proxy-https service, certificate and letsencrypt communication) in order to get lets encrypt to connect.  This had to be done twice. It’s unnerving, but it works.

### GitLab Runner installation

- INSTALL GITLAB-RUNNER to use the Kubernetes Executor in the cluster docs: https://docs.gitlab.com/runner/install/kubernetes-agent.html
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
```
- runner-chart-values.yaml
```
# The GitLab Server URL (with protocol) that want to register the runner against
# ref: https://docs.gitlab.com/runner/commands/README.html#gitlab-runner-register
#
gitlabUrl: https://gitlab.com/

# The Registration Token for adding new runners to the GitLab Server. This must
# be retrieved from your GitLab instance.
# ref: https://docs.gitlab.com/ce/ci/runners/README.html
#
runnerRegistrationToken: "yG71LJzMC7NMCs-xVYAY"

# For RBAC support:
rbac:
    create: true
    rules:
    - apiGroups: [""]
      resources: ["pods"]
      verbs: ["list", "get", "watch", "create", "delete"]
    - apiGroups: [""]
      resources: ["pods/exec"]
      verbs: ["get", "create"]
    - apiGroups: [""]
      resources: ["pods/attach"]
      verbs: ["create"]
    - apiGroups: [""]
      resources: ["secrets", "configmaps"]
      verbs: ["create", "update", "delete"]
    - apiGroups: [""]
      resources: ["services"]
      verbs: ["create"]
    - apiGroups: [""]
      resources: ["pods/log"]
      verbs: ["get"]

# Run all containers with the privileged flag enabled
# This will allow the docker:dind image to run if you need to run Docker
# commands. Please read the docs before turning this on:
# ref: https://docs.gitlab.com/runner/executors/kubernetes.html#using-dockerdind
runners:
    privileged: true
    executor: kubernetes
```
and install, then run
```
kubectl create clusterrolebinding gitlab-runner-crb --clusterrole=cluster-admin --serviceaccount=gitlab-runner:default
```
to give correct permissions to helm.
- Added tag to runner on Gitlab.com (kubernetes); restricted runner to only execute tagged jobs.

## 10/6/21 Ingress controller; Kasten k10 and grafana

- NGINX Ingress Controller documentation at: https://github.com/kubernetes/ingress-nginx/blob/main/README.md#readme
- Google Cloud shell install:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.2/deploy/static/provider/cloud/deploy.yaml
```
generates the output:
```
namespace/ingress-nginx created
serviceaccount/ingress-nginx created
configmap/ingress-nginx-controller created
clusterrole.rbac.authorization.k8s.io/ingress-nginx created
clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx created
role.rbac.authorization.k8s.io/ingress-nginx created
rolebinding.rbac.authorization.k8s.io/ingress-nginx created
service/ingress-nginx-controller-admission created
service/ingress-nginx-controller created
deployment.apps/ingress-nginx-controller created
ingressclass.networking.k8s.io/nginx created
validatingwebhookconfiguration.admissionregistration.k8s.io/ingress-nginx-admission created
serviceaccount/ingress-nginx-admission created
clusterrole.rbac.authorization.k8s.io/ingress-nginx-admission created
clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx-admission created
role.rbac.authorization.k8s.io/ingress-nginx-admission created
rolebinding.rbac.authorization.k8s.io/ingress-nginx-admission created
job.batch/ingress-nginx-admission-create created
job.batch/ingress-nginx-admission-patch created
```
Command to wait until the service is ready
```
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=120s
```
generates the output:
```
pod/ingress-nginx-controller-6b969597bc-gbs24 condition met
```
- change cert-manager issuer creation yaml to clusterissuer
  - leaving the issuer namespace restricted is what caused failures in previous attempts
- construct file monitoring-ingress.yaml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: monitoring
  namespace: monitoring
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-staging"
spec:
  tls:
  - hosts:
    - cluster.enki-portal.org
    secretName: monitoring-tls
  rules:
  - host: cluster.enki-portal.org
    http:
      paths:
     - path: /grafana
        pathType: Prefix
        backend:
          service:
            name: prometheus-stack-grafana
            port:
              number: 80

kubectl -n monitoring apply -f monitoring-ingress.yaml
```
- kubectl application gives the output:
```
ingress.networking.k8s.io/monitoring created
```
- change staging to prod for letsencrypt certificate
- reapply the yaml
- connect to grafana at https://cluster.enki-portal.org/
- change admin password for grafana to ModelAtTheSpeedOfThought
- create the file kasten-ingress.yaml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kasten-io
  namespace: kasten-io
  annotations:
    kubernetes.io/ingress.class: "nginx"
    #cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - k10.enki-portal.org
    secretName: kasten-io-tls
  rules:
  - host: k10.enki-portal.org
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: gateway
            port:
              number: 8000
```
- and apply it to expose kasten k10 at https://k10.enki-portal.org/k10/
```
kubectl -n kasten-io apply -f kasten-ingress.yaml
```
- Use token generated by k10 to access the pod at the above address

## 10/1/21 JupyterHub update; expose K10; configure K10, Google Cloud Storage

- Google Cloud Storage and K10 configure
  - bucket: enki-cluster-bucket
  - K10 reconfigured location profile: enki-cluster
  - Passphrase for K10 Disaster Recovery (no quotes): “Model at the speed of thought” 
    - Cluster ID (needed during restore): 86a4f7f6-9c47-4e2d-ae1e-c25670f6db0b
  - Configured policies for daily backup of chub, mysql, and k10; weekly backup of other installed apps
  - Gitlab agent and runner (and default) not backed up.
- Updated ENKI-portal/jupyterhub_custom to ammend login page
  - current installed helm chart is 0.9.0; current chart version available is 1.1.3 (which has app 1.4.2)
  - Restart hub on cluster using Google Cloud Shell:
```
helm upgrade --cleanup-on-fail jhub jupyterhub/jupyterhub --version=0.9.0 --namespace jhub --reuse-values
``` 
- Upgrade K10 to point to k10.enki-portal.org/k10/
```
helm upgrade k10 kasten/k10 --namespace=kasten-io \
    --reuse-values \
    --set externalGateway.create=true \
    --set auth.tokenAuth.enabled=true
```
output:
```
Release "k10" has been upgraded. Happy Helming!
NAME: k10
LAST DEPLOYED: Thu Sep 30 17:22:22 2021
NAMESPACE: kasten-io
STATUS: deployed
REVISION: 2
TEST SUITE: None
NOTES:
Thank you for installing Kasten’s K10 Data Management Platform!

Documentation can be found at https://docs.kasten.io/.

How to access the K10 Dashboard:



The K10 dashboard is not exposed externally. To establish a connection to it use the following `kubectl` command:

`kubectl --namespace kasten-io port-forward service/gateway 8080:8000`

The Kasten dashboard will be available at: `http://127.0.0.1:8080/k10/#/

The K10 Dashboard is accessible via a LoadBalancer. Find the service's EXTERNAL IP using:
  `kubectl get svc gateway-ext --namespace kasten-io -o wide`
And use it in following URL
 `http://SERVICE_EXTERNAL_IP/k10/#/`
```
- check status:
```
kubectl get svc gateway-ext --namespace kasten-io -o wide
```
output:
```
NAME          TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE     SELECTOR
gateway-ext   LoadBalancer   10.35.243.194   35.233.150.110   80:30155/TCP   2m23s   service=gateway


sa_secret=$(kubectl get serviceaccount k10-k10 --namespace kasten-io)

Name:                k10-k10
Namespace:           kasten-io
Labels:              app=k10
                     app.kubernetes.io/instance=k10
                     app.kubernetes.io/managed-by=Helm
                     app.kubernetes.io/name=k10
                     helm.sh/chart=k10-4.0.13
                     heritage=Helm
                     release=k10
Annotations:         meta.helm.sh/release-name: k10
                     meta.helm.sh/release-namespace: kasten-io
Image pull secrets:  <none>
Mountable secrets:   k10-k10-token-wh6h5
Tokens:              k10-k10-token-wh6h5
Events:              <none>
```
- login with token generated here:
```
kubectl get secret k10-k10-token-wh6h5 --namespace kasten-io -ojsonpath="{.data.token}{'\n'}" | base64 --decode
```
output:
```
token not displayed here.
```

## 9/30/21 - Backup configuration; Installation of Kasten K10 
Documented at https://docs.kasten.io/latest/install/google/google.html.  

Procedure in Google Cloud Shell:
```
helm repo add kasten https://charts.kasten.io/
```
- Run primer tests:
```
curl https://docs.kasten.io/tools/k10_primer.sh | bash
```
- Create the namespace:
```
kubectl create namespace kasten-io
```
- Set Google Cloud Shell to the cluster project:
```
gcloud config set project pelagic-script-244221
```
output:
```
Updated property [core/project].
```
- Create the service account:
```
myproject=$(gcloud config get-value core/project)
gcloud iam service-accounts create k10-test-sa --display-name "K10 Service Account"
```
output:
```
Created service account [k10-test-sa].
```
- Apply the service account:
```
k10saemail=$(gcloud iam service-accounts list --filter "k10-test-sa" --format="value(email)")
gcloud iam service-accounts keys create --iam-account=${k10saemail} k10-sa-key.json
```
output:
```
created key [0b80f0cc586459bb3a3460e4b8d800ce14333aac] of type [json] as [k10-sa-key.json] for [k10-test-sa@pelagic-script-244221.iam.gserviceaccount.com]
```
- Add the storageAdmin role to the service account
```
gcloud projects add-iam-policy-binding ${myproject} --member serviceAccount:${k10saemail} --role roles/compute.storageAdmin
```
output:
```
Updated IAM policy for project [pelagic-script-244221].
bindings:
- members:
  - serviceAccount:service-300265731176@gcf-admin-robot.iam.gserviceaccount.com
  role: roles/cloudfunctions.serviceAgent
- members:
  - serviceAccount:service-300265731176@compute-system.iam.gserviceaccount.com
  role: roles/compute.serviceAgent
- members:
  - serviceAccount:k10-test-sa@pelagic-script-244221.iam.gserviceaccount.com
  role: roles/compute.storageAdmin
- members:
  - serviceAccount:service-300265731176@container-engine-robot.iam.gserviceaccount.com
  role: roles/container.serviceAgent
- members:
  - serviceAccount:300265731176-compute@developer.gserviceaccount.com
  - serviceAccount:300265731176@cloudservices.gserviceaccount.com
  - serviceAccount:pelagic-script-244221@appspot.gserviceaccount.com
  - serviceAccount:service-300265731176@containerregistry.iam.gserviceaccount.com
  role: roles/editor
- members:
  - serviceAccount:service-300265731176@firebase-rules.iam.gserviceaccount.com
  role: roles/firebaserules.system
- members:
  - user:ghiorso@ofm-research.org
  role: roles/owner
etag: BwXNKtsqqfs=
version: 1
```
- Install K10 with that service account 
```
sa_key=$(base64 -w0 k10-sa-key.json)
helm install k10 kasten/k10 --namespace=kasten-io --set secrets.googleApiKey=$sa_key
```
output:
```
NAME: k10
LAST DEPLOYED: Wed Sep 29 23:42:42 2021
NAMESPACE: kasten-io
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Thank you for installing Kasten’s K10 Data Management Platform!

Documentation can be found at https://docs.kasten.io/.

How to access the K10 Dashboard:

The K10 dashboard is not exposed externally. To establish a connection to it use the following `kubectl` command:

`kubectl --namespace kasten-io port-forward service/gateway 8080:8000`

The Kasten dashboard will be available at: `http://127.0.0.1:8080/k10/#/`
```
- Validate the install:
```
kubectl get pods --namespace kasten-io --watch
```
- Validate dashboard access:
```
kubectl --namespace kasten-io port-forward service/gateway 8080:8000
```
- Or, in Google Cloud Shell:
```
kubectl --namespace kasten-io port-forward service/gateway 8080:8000
Web preview
```
replace the default URL (will look like https://8080-example.appspot.com/?authuser=0) with https://8080-example.appspot.com/k10/ where k10 is the release name under which you installed K10.  

- LOAD BALANCER access can be installed with a Helm upgrade:
```
# example uses Token Authentication method
helm upgrade k10 kasten/k10 --namespace=kasten-io \
    --reuse-values \
    --set externalGateway.create=true \
    --set auth.tokenAuth.enabled=true
```
The K10 dashboard will be available at the /k10/ URL path of the DNS or IP address.

## 9/29/21

- Disabled ingress-nginx controller in cluster (because I could not get it to properly interact with the MySQL installation).
- Installed MySQL and exposed the service using a load balancer (just like Jupyter Hub)
```
kubectl create namespace mysql
kubectl create secret generic mysql-creds --from-literal=ROOT_PASSWORD=changeme -n mysql
```
- Create a mysql ConfigMap: mysql-config.yaml
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql
  namespace: mysql
  labels:
    app: mysql
    version: “5.7.35”
data:
  my.cnf: |
    [mysqld]
    max_connections = 600
    read_buffer_size = 1048576
    net_read_timeout = 360
    net_write_timeout = 360
```
- and apply with:
```
kubectl -n mysql apply -f mysql-config.yaml
```
- Create a PersistentVolumeClaim: mysql-pv.yaml
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-storage
  namespace: mysql
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```
- and apply with:
```
kubectl -n mysql apply -f mysql-pv.yaml
```
- Create a MySQL deployment: mysql-deployment.yaml 
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
  namespace: mysql
  labels:
    app: mysql
    version: “5.7.35”
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: mysql
      version: “5.7.35”
  template:
    metadata:
      labels:
        app: mysql
        version: “5.7.35”
    spec:
      containers:
        - name: mysql
          image: mysql:5.7.35
          args:
            - "--ignore-db-dir=lost+found"
          livenessProbe:
            tcpSocket:
              port: 3306
            initialDelaySeconds: 30
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 2
            timeoutSeconds: 3
          ports:
            - containerPort: 3306
          resources:
            limits:
              cpu: 2000m
              memory: 4Gi
            requests:
              cpu: 300m
              memory: 400Mi
          volumeMounts:
            - name: mysql-storage
              mountPath: "/var/lib/mysql"
              subPath: "mysql" 
#            - name: mysql-conf
#              mountPath: /etc/mysql/conf.d
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mysql-creds
                  key: ROOT_PASSWORD
      volumes:
        - name: mysql-storage
          persistentVolumeClaim:
            claimName: mysql-storage
```
- and apply with: 
```
kubectl -n mysql apply -f mysql-deployment.yaml
```
- Finally, create a MySQL service and expose the service using a loadBalancer: mysql-svc.yaml
```
apiVersion: v1
kind: Service
metadata:
  name: mysql-service
  namespace: mysql
spec:
  selector:
    app: mysql
    version: "5.7"
  ports:
  - protocol: TCP
    port: 3306
    targetPort: 3306
  type: LoadBalancer
```
- and apply with:
```
kubectl -n mysql apply -f mysql-svc.yaml
```
- An external IP is generated, which opens port 3306 and routes traffic to the MySQL pod. This IP is mapped to mysql.enki-portal.org at GoDaddy.com.

## 9/23/21

- Installed kube-prometheus-stack using Helm:
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install -n monitoring helm-stack prometheus-community/kube-prometheus-stack
```
Did not customize values of the stack.  Port forwarding works for access to grafana; choose a prometheus source according to the internal cluster ip and port 9090. Uninstall as:
```
helm uninstall prometheus-stack -n monitoring
```
- Upgraded the GitLab agent serviceAccount to have a cluster-admin role
```
kubectl get rolebindings,clusterrolebindings --all-namespaces  -o custom-columns='KIND:kind,NAMESPACE:metadata.namespace,NAME:metadata.name,SERVICE_ACCOUNTS:subjects[?(@.kind=="ServiceAccount")].name' | grep gitlab-agent
```
gave as output:
```
ClusterRoleBinding   <none>          cilium-alert-read                                      gitlab-agent
ClusterRoleBinding   <none>          gitlab-agent-gitops-read-all                           gitlab-agent
ClusterRoleBinding   <none>          gitlab-agent-gitops-write-all                          gitlab-agent
ClusterRoleBinding   <none>          gitlab-agent-read-binding                              gitlab-agent
ClusterRoleBinding   <none>          gitlab-agent-write-binding                             gitlab-agent
```
Execute:
```
kubectl create clusterrolebinding gitlab-agent-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=default:gitlab-agent
kubectl get clusterrolebinding | grep gitlab-agent
```
output from above:
```
gitlab-agent-cluster-admin-binding                     ClusterRole/cluster-admin                                          12s
gitlab-agent-gitops-read-all                           ClusterRole/gitlab-agent-gitops-read-all                           162d
gitlab-agent-gitops-write-all                          ClusterRole/gitlab-agent-gitops-write-all                          162d
gitlab-agent-read-binding                              ClusterRole/gitlab-agent-read                                      162d
gitlab-agent-write-binding                             ClusterRole/gitlab-agent-write                                     162d
```

- Installed certManager (https://cert-manager.io/docs/installation/kubectl/) as:
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.yaml
```
output from above:
```
customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
namespace/cert-manager created
serviceaccount/cert-manager-cainjector created
serviceaccount/cert-manager created
serviceaccount/cert-manager-webhook created
clusterrole.rbac.authorization.k8s.io/cert-manager-cainjector created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-issuers created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificates created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-orders created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-challenges created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
clusterrole.rbac.authorization.k8s.io/cert-manager-view created
clusterrole.rbac.authorization.k8s.io/cert-manager-edit created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
clusterrole.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-cainjector created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-issuers created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificates created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-orders created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-challenges created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
role.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
role.rbac.authorization.k8s.io/cert-manager:leaderelection created
role.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
rolebinding.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection created
rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
service/cert-manager created
service/cert-manager-webhook created
deployment.apps/cert-manager-cainjector created
deployment.apps/cert-manager created
deployment.apps/cert-manager-webhook created
mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
```
Consult website above for uninstall procedure.

### Tested certManager to expose a service (see: https://cert-manager.io/docs/tutorials/acme/ingress/):

- Create file kuard-deployment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kuard
spec:
  selector:
    matchLabels:
      app: kuard
  replicas: 1
  template:
    metadata:
      labels:
        app: kuard
    spec:
      containers:
      - image: gcr.io/kuar-demo/kuard-amd64:1
        imagePullPolicy: Always
        name: kuard
        ports:
        - containerPort: 8080
```
- Create file kuard-service.yaml
```
apiVersion: v1
kind: Service
metadata:
  name: kuard
spec:
  ports:
  - port: 80
    targetPort: 8080
    protocol: TCP
  selector:
    app: kuard
```
- Execute:
```
kubectl apply -f kuard-deployment.yaml
kubectl apply -f kuard-service.yaml
```
- Create file kuard-ingress-simple.yaml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kuard
  annotations:
    kubernetes.io/ingress.class: "nginx"
    #cert-manager.io/issuer: "letsencrypt-staging"

spec:
  tls:
  - hosts:
    - cluster.enki-portal.org
    secretName: quickstart-example-tls
  rules:
  - host: cluster.enki-portal.org
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: kuard
            port:
              number: 80
``` 
- Test ingress by applying this file:
```
kubectl apply -f  kuard-ingress-simple.yaml 
```
The above creates a secret and names it *quickstart-example-tls*. 
- Wait for completion:
```
kubectl get ingress
```
- Test that you can access (use domain name assigned and public IP of ingress-nginx service)
```
curl -kivL -H 'Host: www.example.com' 'http://203.0.113.2'
```
- Then set up certmanager staging and production issuers.
- Create the file staging-cert-manager.yaml
```
   apiVersion: cert-manager.io/v1
   kind: Issuer
   metadata:
     name: letsencrypt-staging
   spec:
     acme:
       # The ACME server URL
       server: https://acme-staging-v02.api.letsencrypt.org/directory
       # Email address used for ACME registration
       email: ghiorso@ofm-research.org
       # Name of a secret used to store the ACME account private key
       privateKeySecretRef:
         name: letsencrypt-staging
       # Enable the HTTP-01 challenge provider
       solvers:
       - http01:
           ingress:
             class:  nginx

```
- Create the file production-cert-manager.yaml
```
   apiVersion: cert-manager.io/v1
   kind: Issuer
   metadata:
     name: letsencrypt-prod
   spec:
     acme:
       # The ACME server URL
       server: https://acme-v02.api.letsencrypt.org/directory
       # Email address used for ACME registration
       email: ghiorso@ofm-research.org
       # Name of a secret used to store the ACME account private key
       privateKeySecretRef:
         name: letsencrypt-prod
       # Enable the HTTP-01 challenge provider
       solvers:
       - http01:
           ingress:
             class: nginx
```
- Execute the commands:
```
kubectl apply -f staging-cert-manager.yaml
kubectl apply -f production-cert-manager.yaml 
```
- Check on issuers to make sure they have a registered account
```
kubectl describe issuer letsencrypt-staging
```
- Deploy ingress:
- Create the file kuard-ingress.yaml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kuard
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/issuer: "letsencrypt-staging"

spec:
  tls:
  - hosts:
    - cluster.enki-portal.org
    secretName: quickstart-example-tls
  rules:
  - host: cluster.enki-portal.org
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: kuard
            port:
              number: 80
```
- Execute the yaml with:
```
kubectl apply -f  kuard-ingress.yaml 
```
- Once you know it works and has installed a certificate,
```
kubectl get certificate
kubectl describe certificate quickstart-example-tls
kubectl describe secret quickstart-example-tls
```
- you can change from staging to production 
- Create the file kuard-ingress-final.yaml
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kuard
  annotations:
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/issuer: "letsencrypt-prod"

spec:
  tls:
  - hosts:
    - cluster.enki-portal.org
    secretName: quickstart-example-tls
  rules:
  - host: cluster.enki-portal.org
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: kuard
            port:
              number: 80
```
- Execute the commands to apply the yaml
```
kubectl apply -f kuard-ingress-final.yaml
kubectl delete secret quickstart-example-tls
kubectl describe certificate quickstart-example-tls
```
- and all should be up and running!
- Delete everything (including the secret for kuard) in reverse order  

## 9/22/21

- After installing kube-prometheus via manifests and configuring ingress access, a problem developed related to assigning ClusterIP-based services (grafana, alertmanager-main, prometheus-k8s) via ingress on GKE. Such access is disallowed and must be based on loadBalancing or NodeIP.
- Customizing the Prometheus installation is best done using Helm charts, so the manifest installation was removed in Google Cloud Shell via:
```
cd kube-prometheus
kubectl delete --ignore-not-found=true -f manifests/ -f manifests/setup
```
which resulted in the output:
```
alertmanager.monitoring.coreos.com "main" deleted
poddisruptionbudget.policy "alertmanager-main" deleted
prometheusrule.monitoring.coreos.com "alertmanager-main-rules" deleted
secret "alertmanager-main" deleted
service "alertmanager-main" deleted
serviceaccount "alertmanager-main" deleted
servicemonitor.monitoring.coreos.com "alertmanager" deleted
clusterrole.rbac.authorization.k8s.io "blackbox-exporter" deleted
clusterrolebinding.rbac.authorization.k8s.io "blackbox-exporter" deleted
configmap "blackbox-exporter-configuration" deleted
deployment.apps "blackbox-exporter" deleted
service "blackbox-exporter" deleted
serviceaccount "blackbox-exporter" deleted
servicemonitor.monitoring.coreos.com "blackbox-exporter" deleted
secret "grafana-config" deleted
secret "grafana-datasources" deleted
configmap "grafana-dashboard-alertmanager-overview" deleted
configmap "grafana-dashboard-apiserver" deleted
configmap "grafana-dashboard-cluster-total" deleted
configmap "grafana-dashboard-controller-manager" deleted
configmap "grafana-dashboard-k8s-resources-cluster" deleted
configmap "grafana-dashboard-k8s-resources-namespace" deleted
configmap "grafana-dashboard-k8s-resources-node" deleted
configmap "grafana-dashboard-k8s-resources-pod" deleted
configmap "grafana-dashboard-k8s-resources-workload" deleted
configmap "grafana-dashboard-k8s-resources-workloads-namespace" deleted
configmap "grafana-dashboard-kubelet" deleted
configmap "grafana-dashboard-namespace-by-pod" deleted
configmap "grafana-dashboard-namespace-by-workload" deleted
configmap "grafana-dashboard-node-cluster-rsrc-use" deleted
configmap "grafana-dashboard-node-rsrc-use" deleted
configmap "grafana-dashboard-nodes" deleted
configmap "grafana-dashboard-persistentvolumesusage" deleted
configmap "grafana-dashboard-pod-total" deleted
configmap "grafana-dashboard-prometheus-remote-write" deleted
configmap "grafana-dashboard-prometheus" deleted
configmap "grafana-dashboard-proxy" deleted
configmap "grafana-dashboard-scheduler" deleted
configmap "grafana-dashboard-workload-total" deleted
configmap "grafana-dashboards" deleted
deployment.apps "grafana" deleted
service "grafana" deleted
serviceaccount "grafana" deleted
servicemonitor.monitoring.coreos.com "grafana" deleted
prometheusrule.monitoring.coreos.com "kube-prometheus-rules" deleted
clusterrole.rbac.authorization.k8s.io "kube-state-metrics" deleted
clusterrolebinding.rbac.authorization.k8s.io "kube-state-metrics" deleted
deployment.apps "kube-state-metrics" deleted
prometheusrule.monitoring.coreos.com "kube-state-metrics-rules" deleted
service "kube-state-metrics" deleted
serviceaccount "kube-state-metrics" deleted
servicemonitor.monitoring.coreos.com "kube-state-metrics" deleted
prometheusrule.monitoring.coreos.com "kubernetes-monitoring-rules" deleted
servicemonitor.monitoring.coreos.com "kube-apiserver" deleted
servicemonitor.monitoring.coreos.com "coredns" deleted
servicemonitor.monitoring.coreos.com "kube-controller-manager" deleted
servicemonitor.monitoring.coreos.com "kube-scheduler" deleted
servicemonitor.monitoring.coreos.com "kubelet" deleted
clusterrole.rbac.authorization.k8s.io "node-exporter" deleted
clusterrolebinding.rbac.authorization.k8s.io "node-exporter" deleted
daemonset.apps "node-exporter" deleted
prometheusrule.monitoring.coreos.com "node-exporter-rules" deleted
service "node-exporter" deleted
serviceaccount "node-exporter" deleted
servicemonitor.monitoring.coreos.com "node-exporter" deleted
apiservice.apiregistration.k8s.io "v1beta1.metrics.k8s.io" deleted
clusterrole.rbac.authorization.k8s.io "prometheus-adapter" deleted
clusterrole.rbac.authorization.k8s.io "system:aggregated-metrics-reader" deleted
clusterrolebinding.rbac.authorization.k8s.io "prometheus-adapter" deleted
clusterrolebinding.rbac.authorization.k8s.io "resource-metrics:system:auth-delegator" deleted
clusterrole.rbac.authorization.k8s.io "resource-metrics-server-resources" deleted
configmap "adapter-config" deleted
deployment.apps "prometheus-adapter" deleted
poddisruptionbudget.policy "prometheus-adapter" deleted
rolebinding.rbac.authorization.k8s.io "resource-metrics-auth-reader" deleted
service "prometheus-adapter" deleted
serviceaccount "prometheus-adapter" deleted
servicemonitor.monitoring.coreos.com "prometheus-adapter" deleted
clusterrole.rbac.authorization.k8s.io "prometheus-k8s" deleted
clusterrolebinding.rbac.authorization.k8s.io "prometheus-k8s" deleted
prometheusrule.monitoring.coreos.com "prometheus-operator-rules" deleted
servicemonitor.monitoring.coreos.com "prometheus-operator" deleted
poddisruptionbudget.policy "prometheus-k8s" deleted
prometheus.monitoring.coreos.com "k8s" deleted
prometheusrule.monitoring.coreos.com "prometheus-k8s-prometheus-rules" deleted
rolebinding.rbac.authorization.k8s.io "prometheus-k8s-config" deleted
rolebinding.rbac.authorization.k8s.io "prometheus-k8s" deleted
rolebinding.rbac.authorization.k8s.io "prometheus-k8s" deleted
rolebinding.rbac.authorization.k8s.io "prometheus-k8s" deleted
role.rbac.authorization.k8s.io "prometheus-k8s-config" deleted
role.rbac.authorization.k8s.io "prometheus-k8s" deleted
role.rbac.authorization.k8s.io "prometheus-k8s" deleted
role.rbac.authorization.k8s.io "prometheus-k8s" deleted
service "prometheus-k8s" deleted
serviceaccount "prometheus-k8s" deleted
servicemonitor.monitoring.coreos.com "prometheus-k8s" deleted
namespace "monitoring" deleted
customresourcedefinition.apiextensions.k8s.io "alertmanagerconfigs.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "alertmanagers.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "podmonitors.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "probes.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "prometheuses.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "prometheusrules.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "servicemonitors.monitoring.coreos.com" deleted
customresourcedefinition.apiextensions.k8s.io "thanosrulers.monitoring.coreos.com" deleted
clusterrole.rbac.authorization.k8s.io "prometheus-operator" deleted
clusterrolebinding.rbac.authorization.k8s.io "prometheus-operator" deleted
deployment.apps "prometheus-operator" deleted
service "prometheus-operator" deleted
```
note that the namespace, monitoring, was deleted.
- Will now attempt a customized installation of the kube-prometheus-stack using Helm via the Google Cloud Shell (the agent is not up to this task)
- The NGINX example manifest is also deleted from this repository, as it is not being utilized. 

## 9/20/21

- Altered configuration of agent to adopt applications currently running in the cluster (see agent config file)
```
inventory_policy: adopt_if_no_inventory
```
- Altered glob in agent config file to only search specific directories for manifest files
```
- glob: 'generated-manifests/**/*.{yaml,yml,json}'
```
- Implemented CI for manifest file generation using Helm templating
- Added manifests to managae knative installation - works!
- Attempted to add the Kube-Prometheus-Stack for cluster monitoring by templating the Helm chart and allowing the agent to install
  - This procedure failed; failure is always related to a permissions issue.  The chart appears to not be creating ClusterRole or ClusterRoleBinding permissions related to installation of webhooks.
  - Disabled the webhook feature. Agent manifest installation still fails because prometheus-operator could not correctly configure rules.
  - Tried installation of the chart manually on the cluster. Similar failures.
  - Conclusion: abandon chart based installation.
- Installed via the agent ingress-nginx controller in anticipation of exposing prometheus and other apps using ingress. 
  - Used ingress-nginx deploy manifest and the agent. Works perfectly.
- Next, uninstalled kube-prometheus-stack from the cluster and removed all manifest file generation from the CI in this repository.

Procedure for this process (it's complicated):

First, disable the agent on the cluster:
- Delete the gitlab-agent from the Kubernetes/Workloads menu on Google Cloud cluster Interface.  This will remove all pods and associated services.
- Important! Delete the inventory list of the agent. Find this under Kubernetes/Configuration on Google Cloud cluster.  This inventory is in the default namespace.  File name is Inventory...

Second, from Google Cloud shell delete all resources running on the cluster associated with prometheus (assumption: prometheus resources are installed in the monitoring namespace):

```
kubectl delete namespace monitoring

kubectl delete crd alertmanagerconfigs.monitoring.coreos.com
kubectl delete crd alertmanagers.monitoring.coreos.com
kubectl delete crd podmonitors.monitoring.coreos.com
kubectl delete crd probes.monitoring.coreos.com
kubectl delete crd prometheuses.monitoring.coreos.com
kubectl delete crd prometheusrules.monitoring.coreos.com
kubectl delete crd servicemonitors.monitoring.coreos.com
kubectl delete crd thanosrulers.monitoring.coreos.com

kubectl delete Clusterrole prometheus-operator
kubectl delete ClusterroleBinding prometheus-operator
```
Potentially, there are additional resources that may exist from previous attempts at installation.  If the install procedure below fails, just delete them.  Keep deleting these resources until success is achieved.

Third, clone the kube-prometheus master repository in Google Cloud Shell to gain access to deploy manifest files for direct installation:

```
it clone https://github.com/prometheus-operator/kube-prometheus
cd kube-prometheus
```

Fourth, execute an installation using example default manifests:
```
kubectl create -f manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl create -f manifests/
```

Fifth, test the installation using port-forwarding of Grafana, Alertmanager, and Prometheus-operator as described in the above repository README.  

Sixth, reinstall the Gitlab agent to pickup existing controlled manifests, but not currently kube-prometheus:
```
docker run --pull=always --rm \
    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
    --agent-token=kj6ePL1FnsKEQ2b_xjEfjyryeL2sxCGuYqXDuX3mccz7NfJvdQ \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent | kubectl apply -f -
``` 

- TBD: Figure out a way to manage and customize kube-prometheus via the agent repository, either by using manifests or my issuing kubectl commands via the agent tunnel.
- TBD: Configure external access to Prometheus and Grafana by installing a link to ingress (https://github.com/prometheus-operator/kube-prometheus#exposing-prometheusalermanagergrafana-via-ingress) or by using a load balancer (as is done with GitHub).

Nest steps:
- Customize prometheus/grafana
- Expose prometheus/grafana
- Install MySQL (https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/)
  - Try this via the agent first
  - Tricky part is that deployment manifests must be run in order; the first must succeed before the other is executed.

## 9/11/21

- Install of NGINX example manifest file
- Installation fails initially
  - Resolved by upgrading Kubernetes cluster to 1.20.8-gke.2100
  - Submitted issue to Gitlab Agent group to update documentation regarding the fact that the manifest requires that the cluster not be running the stable 1.19+ release.
- Attempted to install GitLab runner using the procedure presented in the documentation. This resulted in a series of permission problems that prevented installation.  Abandoned this attempt.  Might return to this if there is a need for the runner.
- Installed Prometheus using the kube-prometheus stack
  - The way this would be done on the cluster is to use the command (with installation using release name "prometheus" into the default namespace):
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/kube-prometheus-stack
```
- Since we can't install using Helm and would rather not install using deployment manifests, a combined deployment manifest was generated using:
```
helm template --namespace monitoring kube-prometheus prometheus-community/kube-prometheus-stack > kube-prometheus-stack-manifest.yaml
```
- where installation takes place in the namespace "monitoring" and the release is named "kube-prometheus."
- The generated manifest was uploaded and committed. The agent  executed it mostly with success. A node resource limitation was encountered and the defaiult node pool was upgraded to autoscale to 5 nodes. This appears to have fixed the issue.
- The cluster now appears to have a working version of prometheus and grafana.  Both remain to be exposed and tested.
- Finally, note the following interesting kubectl commands for managing namespaces and their resources:
```
kubectl create ns gitlab-runner
kubectl delete all --all -n {namespace}
``` 

## 9/6/2021

Notes below the horizontal line pertain to prototype version of this repository, which is now archived.  It implemented the initial Kubernetes agent installation method. That repository was disconnected from teh cluster and archived.  

### Roadmap: 
- Install Kubernetes agent in Gitlab 
- Consider a GitLab managed cluster project for cloud apps
  -  Consider Terraform
- Consider K10 for Kubernetes backup issues
- Consider User billing and tracking (Kubecost)
- Investigate project use of GitLab runner
- Consider Runbooks using Rubix/Nurtch) 
  - Cloudwatch Integration
  - Elastic Container service
  - Kubernetes (comes preinstalled with JupyterHub in GitLab 11.4+)
- Investigate serverless platform and port MELTS web services
  - Knative?
  - Google Cloud Run?

### What was done to this repository:  

On the Google cluster associated with ThermoEngine project
- At Infrastructure->Clusters connected with certificate click on ENKI GKE Cluster
- Integrations panel, uncheck Prometheus integration (save changes)
- At Settings->Integrations, verified that Prometheus integration is no longer active
- At Infrastructure->Clusters connected with certificate click on ENKI GKE Cluster
  - Advanced settings panel 
    - Click on “Clear cluster cache”
    - Click on “Remove integration and resources”
- Cluster integration is now removed.

- kubernetes-agent project (version 1, now replaced) 
- In GraphiQL
```
	mutation deleteAgent {
  		clusterAgentDelete(input: {id: "gid://gitlab/Clusters::Agent/96"}) {
    			errors
  		}
	}
```
- On the Google Cloud platform 
  - deleted the Gitlab-agent (two versions, one in gitlab-agent namespace, one pending in the default namespace)
  - deleted agent configuration files from the Configuration 
- At Gitlab
	- New blank project: GKE-ENKI-GitLab-agent in ENKI-portal (public)
	- Add configuration document from previous ghiorso/kubernetes-agent project
	- Archive ghiorso/kubernetes-agent project
	- Add new file .gitlab/agents/primary-agent/config.yaml
	- (in this project) Infrastructure->Kubernetes clusters->GitLab Agent managed clusters
		- “Integrate with the GitLab Agent
```
token generated: kj6ePL1FnsKEQ2b_xjEfjyryeL2sxCGuYqXDuX3mccz7NfJvdQ
```
- Cloud Shell commands:
  - connection initially failed because kubeconfig file pointed to wrong cluster
  - trick: after delete of .kube directory run
	  - gcloud init
	  - gcloud container clusters get-credentials cluster-name
```		
docker run --pull=always --rm \
    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
    --agent-token=kj6ePL1FnsKEQ2b_xjEfjyryeL2sxCGuYqXDuX3mccz7NfJvdQ \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent | kubectl apply -f -
```

## 4/9/2021  
Prometheus was previously install as a managed app on the ENKIserver cluster. This will be changed to an integrated service, which will allow linkage to a more recent installation of Prometheus on teh cluster that is self managed and includes Grafana (i.e. the "kube-prometheus-stack installation via Helm).

- On the Gitlab repository portal, operations->kubernetes
  - Click on ENKIserver cluster and choose the *Applications* tab
  - Uninstall Gitlab Runner
  - Uninstall Prometheus
  - Now, "Enable Prometheus Integration" appears as an option on the Integrations tab. Follow the instructions found in the link "documented process."
  
```
# Create the require Kubernetes namespace
kubectl create ns gitlab-managed-apps

# Download Helm chart values that is compatible with the requirements above.
# You should substitute the tag that corresponds to the GitLab version in the url
# - https://gitlab.com/gitlab-org/gitlab/-/raw/<tag>/vendor/prometheus/values.yaml
#
wget https://gitlab.com/gitlab-org/gitlab/-/raw/v13.9.0-ee/vendor/prometheus/values.yaml

# Add the Prometheus community helm repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# Install Prometheus
helm install prometheus prometheus-community/prometheus -n gitlab-managed-apps --values values.yaml
```

These commands returned:
```
NAME: prometheus
LAST DEPLOYED: Fri Apr  9 16:52:20 2021
NAMESPACE: gitlab-managed-apps
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The Prometheus server can be accessed via port 80 on the following DNS name from within your cluster:
prometheus-prometheus-server.gitlab-managed-apps.svc.cluster.local


Get the Prometheus server URL by running these commands in the same shell:
  export POD_NAME=$(kubectl get pods --namespace gitlab-managed-apps -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace gitlab-managed-apps port-forward $POD_NAME 9090


#################################################################################
######   WARNING: Pod Security Policy has been moved to a global property.  #####
######            use .Values.podSecurityPolicy.enabled with pod-based      #####
######            annotations                                               #####
######            (e.g. .Values.nodeExporter.podSecurityPolicy.annotations) #####
#################################################################################



For more information on running Prometheus, visit:
https://prometheus.io/
```
And, clicking the integration checkbox on the GitLab repository interface gives successful integration.

Next, go to Google Cloud dashboard, Kubernetes
- Services & Ingress, click on prometheus-prometheus-server
- Click on prometheus-prometheus-server under deployments
    - From the actions menu, choose Expose
    - Set target port at 9090, exposed port at 60000
    - Set IP to load balancing
    - Expost the port
- Goto Grafana Lab (set up free account previously)
    - Add prometheus data source correcponding to exposed port above)
    - Add Dashboards
- Return to GitLab.com repository interface
    - In settings->operations, open "metric dashboards" and add https://enkiserver.grafana.com as a source

## 4/12/21

[Kubernetes Agent Installation](https://docs.gitlab.com/ee/user/clusters/agent/#define-a-configuration-repository)

- Create a repository called *kubernetes-agent* at GitLab.com
- Add .gitlab/agents/enki_agent/config.yaml file
```
gitops:
  manifest_projects:
  - id: "ghiorso/kubernetes-agent"
```
- Create an agent record in [GraphiQL](https://gitlab.com/-/graphql-explorer). Two stages. Stage 1:
```
mutation createAgent {
    # agent-name should be the same as specified above in the config.yaml
    createClusterAgent(input: { projectPath: "ghiorso/kubernetes-agent", name: "enki-agent" }) {
      clusterAgent {
        id
        name
      }
      errors
    }
  }
```
Output:
```
{
  "data": {
    "createClusterAgent": {
      "clusterAgent": {
        "id": "gid://gitlab/Clusters::Agent/96",
        "name": "enki-agent"
      },
      "errors": []
    }
  }
}
```
- Stage 2 (generic record):
```
mutation createToken {
    clusterAgentTokenCreate(
      input: {
        clusterAgentId: "<cluster-agent-id-taken-from-the-previous-mutation>"
        description: "<optional-description-of-token>"
        name: "<required-name-given-to-token>"
      }
    ) {
      secret # This is the value you need to use on the next step
      token {
        createdAt
        id
      }
      errors
    }
  }
```
- Stage 2 (specific record):
```
mutation createToken {
    clusterAgentTokenCreate(
      input: {
        clusterAgentId: "gid://gitlab/Clusters::Agent/96"
        description: "ENKI GKE Cluster"
        name: "enki-agent"
      }
    ) {
      secret # This is the value you need to use on the next step
      token {
        createdAt
        id
      }
      errors
    }
}
```
Output:
```
{
  "data": {
    "clusterAgentTokenCreate": {
      "secret": "itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew",
      "token": {
        "createdAt": "2021-04-13T03:44:30Z",
        "id": "gid://gitlab/Clusters::AgentToken/95"
      },
      "errors": []
    }
  }
}
```
- In cloud shell on the Google Cloud dashboard:
    - Image container registry:  https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/container_registry
    - most recent cli container is v13.10.1
    - run the command (generic):
    ```
    docker run \
    --pull=always \
    --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable \
    generate \
    --agent-token=your-agent-token \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable | kubectl apply -f -
    ```
    - (specific)
    ```
    docker run \
    --pull=always \
    --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 \
    generate \
    --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew \
    --kas-address=wss://kas.gitlab.com \
    --agent-version v13.10.1 | kubectl apply -f -
    ```
Output:
```
docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 generate --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew --kas-address=wss://kas.gitlab.com --agent-version v13.10.1 | kubectl apply -f -
v13.10.1: Pulling from gitlab-org/cluster-integration/gitlab-agent/cli
d94d38b8f0e6: Pulling fs layer
c3c40d5e8c96: Pulling fs layer
0f41bae7d828: Pulling fs layer
828e2783a94d: Pulling fs layer
3350ab7677e9: Pulling fs layer
db8e99d0a931: Pulling fs layer
94d03d3ba937: Pulling fs layer
828e2783a94d: Waiting
3350ab7677e9: Waiting
db8e99d0a931: Waiting
94d03d3ba937: Waiting
d94d38b8f0e6: Download complete
0f41bae7d828: Verifying Checksum
0f41bae7d828: Download complete
c3c40d5e8c96: Verifying Checksum
c3c40d5e8c96: Download complete
d94d38b8f0e6: Pull complete
db8e99d0a931: Verifying Checksum
db8e99d0a931: Download complete
c3c40d5e8c96: Pull complete
828e2783a94d: Verifying Checksum
828e2783a94d: Download complete
0f41bae7d828: Pull complete
94d03d3ba937: Verifying Checksum
94d03d3ba937: Download complete
3350ab7677e9: Verifying Checksum
3350ab7677e9: Download complete
828e2783a94d: Pull complete
3350ab7677e9: Pull complete
db8e99d0a931: Pull complete
94d03d3ba937: Pull complete
Digest: sha256:c2f50d1f01a3bed5b0fe3a487dbbf7e3f5e6e944dc1713c99e2318392737ca4f
Status: Downloaded newer image for registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1
clusterrole.rbac.authorization.k8s.io/cilium-alert-read created
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all created
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all created
clusterrolebinding.rbac.authorization.k8s.io/cilium-alert-read created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all created
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found
Error from server (NotFound): error when creating "STDIN": namespaces "gitlab-agent" not found

ghiorso@cloudshell:~ (pelagic-script-244221)$ kubectl create namespace gitlab-agent
namespace/gitlab-agent created

ghiorso@cloudshell:~ (pelagic-script-244221)$ docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1 generate --agent-token=itQE4zJYu5WNY-T2whC-WzVRWCGAwhArmzyey-SUchQMusxzew --kas-address=wss://kas.gitlab.com --agent-version v13.10.1 | kubectl apply -f -

v13.10.1: Pulling from gitlab-org/cluster-integration/gitlab-agent/cli
Digest: sha256:c2f50d1f01a3bed5b0fe3a487dbbf7e3f5e6e944dc1713c99e2318392737ca4f
Status: Image is up to date for registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:v13.10.1
serviceaccount/gitlab-agent created
clusterrole.rbac.authorization.k8s.io/cilium-alert-read unchanged
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all unchanged
clusterrole.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all unchanged
clusterrolebinding.rbac.authorization.k8s.io/cilium-alert-read unchanged
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-read-all unchanged
clusterrolebinding.rbac.authorization.k8s.io/gitlab-agent-gitops-write-all unchanged
secret/gitlab-agent-token-khm2ff582k created
deployment.apps/gitlab-agent created
```
Note that in the above instructions from GitLab docs, an error occurs because the *gitlab-agent* namespace does not exist. It should be created first.

## Not Yet Implemented

This installation is intended for self-managed repositories.
    
Install manifest file for GitLab runner
- Make a [GitLab Chart YAML file](https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/master/values.yaml), like *runner-chart-values.yaml*:
```
# The GitLab Server URL (with protocol) that want to register the runner against
# ref: https://docs.gitlab.com/runner/commands/README.html#gitlab-runner-register
#
gitlabUrl: https://gitlab.my.domain.example.com/

# The Registration Token for adding new runners to the GitLab Server. This must
# be retrieved from your GitLab instance.
# ref: https://docs.gitlab.com/ce/ci/runners/README.html
#
runnerRegistrationToken: "yrnZW46BrtBFqM7xDzE7dddd"

# For RBAC support:
rbac:
    create: true

# Run all containers with the privileged flag enabled
# This will allow the docker:dind image to run if you need to run Docker
# commands. Please read the docs before turning this on:
# ref: https://docs.gitlab.com/runner/executors/kubernetes.html#using-dockerdind
runners:
    privileged: true
```
- Create a single manifest file to install the GitLab Runner chart with your cluster agent, replacing GITLAB GITLAB-RUNNER with your namespace:
```
helm template --namespace GITLAB GITLAB-RUNNER -f runner-chart-values.yaml gitlab/gitlab-runner > runner-manifest.yaml
```
- Push your runner-manifest.yaml to your manifest repository
