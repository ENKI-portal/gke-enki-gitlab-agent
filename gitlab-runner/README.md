# GitLab Runner installation
This GitLab Runner works in conjunction with the GitLab Kubernetes Agent (GKA) to run GitOps jobs using the Kubernetes Executor on the Kubernetes cluster where the agent is installed (the ENKI Google Kubernetes Cluster).  Although stated otherwise in the GitLab documentation (as found in October 2021), the GKA cannot properly install the runner onto the cluster.  The procedure below uses Google Cloud Shell for the installation.  

Refer to the sometimes useful documentation at https://docs.gitlab.com/runner/install/kubernetes-agent.html .  

##### To install and set up GitLab Runner:
1. In Google Cloud Shell, configure *helm* to access GitLab charts:
    ```
    helm repo add gitlab https://charts.gitlab.io
    helm repo update
    ```
1. Configure/Update chart values stored in the file *runner-chart-values.yaml*. 
   
   Note that the environment variable, GITLAB_RUNNER_TOKEN, should be set or updated to the value of the GitLab Runner registration token generated in Settings > CI/CD > Runners > Specific runners for this project.  
    
1. Install with the command:
    ```
    helm upgrade --cleanup-on-fail --install gitlab-runner gitlab/gitlab-runner \
        --namespace gitlab-runner --create-namespace --values runner-chart-values.yaml
    ```
1. Update the runner's service account so that it can execute configuration commands in any required namespace on the cluster:
    ```
    kubectl create clusterrolebinding gitlab-runner-crb --clusterrole=cluster-admin \
    --serviceaccount=gitlab-runner:default
    ```
    
1. In the GitLab IDE for this project, add a tag to the runner called **kubernetes** and restrict it to execute only jobs with this tag.
