# Google Cloud Storage

Google Cloud Storage (GCS) is required for *Kasten K10* cluster backup configuration.  GCS is configured using the Google Cloud IDE and is external to the Kubernetes cluster.  The cluster relevant bucket name for storage is **enki-cluster-bucket**.  This storage is used exclusively by *Kasten K10*.
  
  